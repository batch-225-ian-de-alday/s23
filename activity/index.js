let trainer = {
  name: `Ash Ketchum`,
  age: 10,
  pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasor'],
  friends: {
      Hoenn: ["Ian", "Tracy"],
      Kanto: ["Brock", "Misty"]
   }

}

trainer.talk = function() {
    console.log(`Pikachu! I choose you!`)
}

console.log(trainer)

//using dot
console.log(`Result of dot noation`);
console.log(trainer.name);

//using square bracket notation
console.log(`Result of Square bracket noation`);
console.log(trainer['pokemon']);

//invoke talk function
console.log(`Result of Talk Method`);
trainer.talk()

//8.
function Pokemon(name, level, health, attack,) {
    this.name = name;
    this.level = level;
    this.health = health;
    this.attack = attack;
    

    //10-11. methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        
        console.log( target.name + "'s health is now reduced to " + target.health);

        let isZeroHp = target.health <= 0;
        if (isZeroHp) {
            target.faint()
        }
    };

    this.faint = function() {
        console.log(`${this.name} fainted.`);
    }
    // }
}


//9.
let pikachu_pokemon = new Pokemon(`Pikachu`, 12, 24, 12);
console.log(pikachu_pokemon);

let gueodude = new Pokemon(`Gueodude`, 8, 16, 8);
console.log(gueodude);

let mwetwo_pokemon = new Pokemon(`Mewtwo`, 100, 200, 100);
console.log(mwetwo_pokemon);


// 12-13.

gueodude.tackle(pikachu_pokemon);
console.log(pikachu_pokemon);
mwetwo_pokemon.tackle(gueodude);
console.log(gueodude);













/*console.log(`Pokemon Game:`)

  function Pokemon(name, level) {

      // Properties
      this.name = name;
      this.level = level;
      this.health = 2 * level;
      this.attack = level;

      //Methods
      this.tackle = function(target) {
          console.log(this.name + ' tackled ' + target.name);
          target.health -= this.attack;
          console.log( target.name + "'s health is now reduced to " + target.health);
          };
      this.faint = function(){
          console.log(this.name + 'fainted.');
      }

  }

  // Creates new instances of the "Pokemon" object each with their unique properties
  let pikachu = new Pokemon("Pikachu", 3);
  let rattata = new Pokemon('Rattata', 8); //16 health

  // Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata);*/