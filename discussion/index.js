console.log("Gooo!");

//[SECTION] OBJECTS
/*
 	- An Object is a data type that is used to represent real world objects.
	- Information stored in objects are represented in a"key:value" pair
	- A "key" is also mostly reffered to as a "Property of an object"
	- Different data types may be stored in an object's property creating complex data structure

		Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999,
	price: 100
};

console.log("Result from creating objects");
console.log(cellphone);
console.log(typeof cellphone);


// Creating objects using a construction function

/*
	-creating a reusable function to create several objects that have the same data structure.
	-this is useful for creating multiple instances/ copies of an object.
	- An instance is a concrete occurence of any object which emphasize on the distinct/ unique identity.

	Syntax:
	 function ObjectName(keyA, keyB) {
			this.keyA = keyA;
			this.keyB = keyB;
			 }
*/

// the "This" keyword allows to assign a new objects properties by associating them with values recieved from a constructor function's parameters

function Laptop(name, manufactureDate, price) {
	this.name = name;
	this.manufactureDate = manufactureDate;	
	this.price = price;
}

// the 'new' operator creates an instance if an object.

let laptop = new Laptop('Lenevo', 2008, 20000);
console.log("Result	from creating objects using object contructors:");
console	.log(laptop);


let myLaptop = new Laptop('MacBook Air', 2020, 30000);
console.log("Result	from creating objects using object contructors:");
console	.log(myLaptop);


// [SECTION] Accessing object Properties

//using dot notaion: Best Practice
console.log(`Result from dot notataion ${myLaptop.name}`);

// using the square bracket notation - bad practice
console.log(`Result from square bracket notataion: ${myLaptop['name']}`)



// Accessing array objects
/*
	- Accessing object properties using the square bracket notation and indexes can cause confusion.
	- By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects.
	
	Syntax: 
		array/variable[index].name

*/

//     let laptop = new Laptop('Lenovo', 2008);
//     let myLaptop = new Laptop('MacBook Air', 2020);

let array = [laptop, myLaptop];

// console.log(array[0]['price']);
console.log(array[0].price);

let sample = ['Hello', {firstName: 'John', lastName: 'Doe'}, 78];

console.log(sample);
console.log(sample[1]);
console.log(sample[1].firstName); // to access the object inside an array




// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
/*
    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    - This is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};


// Initializing/adding object properties using dot notation
car.name = 'Honda Civic';
console.log('Result from adding properties using dot notation:');
console.log(car);


// Initializing/adding object properties using bracket notation
/*
    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation

	- This also makes names of object properties to not 
	follow commonly used naming conventions for them
*/


// adding object properties

// car.manufacture_date = 2019;
// using dot notation must not use space underscore is best practice

car['manufacture date'] = 2019;
console.log('Result from adding properties using bracket ntoation:');
console.log(car);


// deleting object properties
delete car ['manufacture date'];
console.log(`Result deleting properties:`);
console.log(car);


// Reassigning object properties
car.name = 'Dodge Charger R/T';
console.log(`Result from reassinging properties:`);
console.log(car);



// [Section] Object Methods
/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

// Note: If you access an object method without (), it will return the function definition:

// function() { return this.firstName + " " + this.lastName; }

let person = {
    name: 'John',
    talk: function (){
        console.log('Hello my name is ' + this.name);
    }
}

console.log(person);
console.log('Result from object methods:');
person.talk();
// console.log(person.talk()) another method

// Adding methods to objects
person.walk = function() { 
    console.log(this.name + ' walked 25 steps forward.');
};
person.walk();
console.log(person);

// Methods are useful for creating reusable functions that perform tasks related to objects
let friend = {
    firstName: 'Joe',
    lastName: 'Smith',
    address: {
        city: 'Austin',
        country: 'Texas'
    },
    emails: ['joe@mail.com', 'joesmith@email.xyz'],
    introduce: function() {
        console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
    }
};
console.log(friend);//showing the friend object
friend.introduce();
console.log(friend.address.city)//accessing object over an object

console.log(friend.emails[1]);//accessing array on an object

console.log(friend.emails[0], friend.emails[1]) // accessing emails on array 0 and 3 index

// console.log(friend.emails.splice(0,1))//using splice in array of object





// [SECTION] Real World Application Objects

/*
	-Scenario
	1. We would like to create a game that would have pokemon interact with each other.
	2. Every pokemon would have the same set of stats, properties and function.
*/


  // Creating an object constructor instead will help with this process

console.log(`Pokemon Game:`)

  function Pokemon(name, level) {

      // Properties
      this.name = name;
      this.level = level;
      this.health = 2 * level;
      this.attack = level;

      //Methods
      this.tackle = function(target) {
          console.log(this.name + ' tackled ' + target.name);
          target.health -= this.attack;
          console.log( target.name + "'s health is now reduced to " + target.health);
          };
      this.faint = function(){
          console.log(this.name + 'fainted.');
      }

  }

  // Creates new instances of the "Pokemon" object each with their unique properties
  let pikachu = new Pokemon("Pikachu", 3);
  let rattata = new Pokemon('Rattata', 8); //16 health

  // Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
pikachu.tackle(rattata);